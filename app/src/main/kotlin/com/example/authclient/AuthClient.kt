package com.example.authclient

import io.grpc.ManagedChannel
import java.io.Closeable
import java.util.concurrent.TimeUnit

class AuthClient(private val channel: ManagedChannel) : Closeable {
    private val stub: AuthServiceRoutesGrpcKt.AuthServiceRoutesCoroutineStub =
        AuthServiceRoutesGrpcKt.AuthServiceRoutesCoroutineStub(channel)

    suspend fun login(username: String, password: String): String {
        val accountCredentials = AccountCredentials
            .newBuilder()
            .setUsername(username)
            .setPassword(password)
            .build()

        val response = stub.login(accountCredentials)

        val oauthCredentials = OauthCredentials
            .newBuilder()
            .setToken(response.token)
            .setTimeoutSeconds(response.timeoutSeconds)
            .build()

        return oauthCredentials.token
    }

    suspend fun logout(oauthToken: String): String {
        val oauthCredentials = OauthCredentials
            .newBuilder()
            .setToken(oauthToken)
            .build()

        val response = stub.logout(oauthCredentials)

        return response.token
    }

    override fun close() {
        channel.shutdown().awaitTermination(5, TimeUnit.SECONDS)
    }

}