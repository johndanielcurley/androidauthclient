package com.example.authclient

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.authclient.databinding.ActivityMainBinding
import io.grpc.ManagedChannel
import io.grpc.android.AndroidChannelBuilder
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class MainActivity : AppCompatActivity() {
    private val port = 50051
    private val host = "0.0.0.0"
    private lateinit var binding: ActivityMainBinding
    private var token = ""

    private val androidChannel: ManagedChannel = AndroidChannelBuilder
        .forAddress(host, port)
        .usePlaintext()
        .build()


    private val client = AuthClient(androidChannel)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        loginHandler()
        logoutHandler()
    }

    private fun loginHandler() {
        binding.loginButton.setOnClickListener {
            CoroutineScope(Dispatchers.IO).launch {
                try {
                    token = ""
                    val email = binding.emailInput.text.toString()
                    val password = binding.passwordInput.text.toString()
                    token = client.login(username = email, password = password)
                    withContext(Dispatchers.Main) {
                        binding.tokenText.text = token
                    }
                } catch (e: Exception) {
                    Handler(Looper.getMainLooper()).post {
                        Toast.makeText(applicationContext, e.message, Toast.LENGTH_LONG).show()
                    }
                }
            }
        }
    }

    private fun logoutHandler() {
        binding.logoutButton.setOnClickListener {
            CoroutineScope(Dispatchers.IO).launch {
                try {
                    token = client.logout(binding.tokenText.text.toString())
                    withContext(Dispatchers.Main) {
                        binding.tokenText.text = token
                    }
                } catch (e: Exception) {
                    Handler(Looper.getMainLooper()).post {
                        Toast.makeText(applicationContext, e.message, Toast.LENGTH_LONG).show()
                    }
                }
            }
        }
    }
}
