package com.example.authclient;

import static io.grpc.MethodDescriptor.generateFullMethodName;

/**
 * <pre>
 * Define the service containing methods here
 * </pre>
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.42.1)",
    comments = "Source: authService.proto")
@io.grpc.stub.annotations.GrpcGenerated
public final class AuthServiceRoutesGrpc {

  private AuthServiceRoutesGrpc() {}

  public static final String SERVICE_NAME = "authService.AuthServiceRoutes";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<com.example.authclient.AccountCredentials,
      com.example.authclient.OauthCredentials> getLoginMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "Login",
      requestType = com.example.authclient.AccountCredentials.class,
      responseType = com.example.authclient.OauthCredentials.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<com.example.authclient.AccountCredentials,
      com.example.authclient.OauthCredentials> getLoginMethod() {
    io.grpc.MethodDescriptor<com.example.authclient.AccountCredentials, com.example.authclient.OauthCredentials> getLoginMethod;
    if ((getLoginMethod = AuthServiceRoutesGrpc.getLoginMethod) == null) {
      synchronized (AuthServiceRoutesGrpc.class) {
        if ((getLoginMethod = AuthServiceRoutesGrpc.getLoginMethod) == null) {
          AuthServiceRoutesGrpc.getLoginMethod = getLoginMethod =
              io.grpc.MethodDescriptor.<com.example.authclient.AccountCredentials, com.example.authclient.OauthCredentials>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "Login"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.lite.ProtoLiteUtils.marshaller(
                  com.example.authclient.AccountCredentials.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.lite.ProtoLiteUtils.marshaller(
                  com.example.authclient.OauthCredentials.getDefaultInstance()))
              .build();
        }
      }
    }
    return getLoginMethod;
  }

  private static volatile io.grpc.MethodDescriptor<com.example.authclient.OauthCredentials,
      com.example.authclient.OauthCredentials> getLogoutMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "Logout",
      requestType = com.example.authclient.OauthCredentials.class,
      responseType = com.example.authclient.OauthCredentials.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<com.example.authclient.OauthCredentials,
      com.example.authclient.OauthCredentials> getLogoutMethod() {
    io.grpc.MethodDescriptor<com.example.authclient.OauthCredentials, com.example.authclient.OauthCredentials> getLogoutMethod;
    if ((getLogoutMethod = AuthServiceRoutesGrpc.getLogoutMethod) == null) {
      synchronized (AuthServiceRoutesGrpc.class) {
        if ((getLogoutMethod = AuthServiceRoutesGrpc.getLogoutMethod) == null) {
          AuthServiceRoutesGrpc.getLogoutMethod = getLogoutMethod =
              io.grpc.MethodDescriptor.<com.example.authclient.OauthCredentials, com.example.authclient.OauthCredentials>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "Logout"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.lite.ProtoLiteUtils.marshaller(
                  com.example.authclient.OauthCredentials.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.lite.ProtoLiteUtils.marshaller(
                  com.example.authclient.OauthCredentials.getDefaultInstance()))
              .build();
        }
      }
    }
    return getLogoutMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static AuthServiceRoutesStub newStub(io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<AuthServiceRoutesStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<AuthServiceRoutesStub>() {
        @java.lang.Override
        public AuthServiceRoutesStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new AuthServiceRoutesStub(channel, callOptions);
        }
      };
    return AuthServiceRoutesStub.newStub(factory, channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static AuthServiceRoutesBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<AuthServiceRoutesBlockingStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<AuthServiceRoutesBlockingStub>() {
        @java.lang.Override
        public AuthServiceRoutesBlockingStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new AuthServiceRoutesBlockingStub(channel, callOptions);
        }
      };
    return AuthServiceRoutesBlockingStub.newStub(factory, channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static AuthServiceRoutesFutureStub newFutureStub(
      io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<AuthServiceRoutesFutureStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<AuthServiceRoutesFutureStub>() {
        @java.lang.Override
        public AuthServiceRoutesFutureStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new AuthServiceRoutesFutureStub(channel, callOptions);
        }
      };
    return AuthServiceRoutesFutureStub.newStub(factory, channel);
  }

  /**
   * <pre>
   * Define the service containing methods here
   * </pre>
   */
  public static abstract class AuthServiceRoutesImplBase implements io.grpc.BindableService {

    /**
     * <pre>
     * Basic function call, makes request and returns value
     * </pre>
     */
    public void login(com.example.authclient.AccountCredentials request,
        io.grpc.stub.StreamObserver<com.example.authclient.OauthCredentials> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getLoginMethod(), responseObserver);
    }

    /**
     */
    public void logout(com.example.authclient.OauthCredentials request,
        io.grpc.stub.StreamObserver<com.example.authclient.OauthCredentials> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getLogoutMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getLoginMethod(),
            io.grpc.stub.ServerCalls.asyncUnaryCall(
              new MethodHandlers<
                com.example.authclient.AccountCredentials,
                com.example.authclient.OauthCredentials>(
                  this, METHODID_LOGIN)))
          .addMethod(
            getLogoutMethod(),
            io.grpc.stub.ServerCalls.asyncUnaryCall(
              new MethodHandlers<
                com.example.authclient.OauthCredentials,
                com.example.authclient.OauthCredentials>(
                  this, METHODID_LOGOUT)))
          .build();
    }
  }

  /**
   * <pre>
   * Define the service containing methods here
   * </pre>
   */
  public static final class AuthServiceRoutesStub extends io.grpc.stub.AbstractAsyncStub<AuthServiceRoutesStub> {
    private AuthServiceRoutesStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected AuthServiceRoutesStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new AuthServiceRoutesStub(channel, callOptions);
    }

    /**
     * <pre>
     * Basic function call, makes request and returns value
     * </pre>
     */
    public void login(com.example.authclient.AccountCredentials request,
        io.grpc.stub.StreamObserver<com.example.authclient.OauthCredentials> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getLoginMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void logout(com.example.authclient.OauthCredentials request,
        io.grpc.stub.StreamObserver<com.example.authclient.OauthCredentials> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getLogoutMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   * <pre>
   * Define the service containing methods here
   * </pre>
   */
  public static final class AuthServiceRoutesBlockingStub extends io.grpc.stub.AbstractBlockingStub<AuthServiceRoutesBlockingStub> {
    private AuthServiceRoutesBlockingStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected AuthServiceRoutesBlockingStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new AuthServiceRoutesBlockingStub(channel, callOptions);
    }

    /**
     * <pre>
     * Basic function call, makes request and returns value
     * </pre>
     */
    public com.example.authclient.OauthCredentials login(com.example.authclient.AccountCredentials request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getLoginMethod(), getCallOptions(), request);
    }

    /**
     */
    public com.example.authclient.OauthCredentials logout(com.example.authclient.OauthCredentials request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getLogoutMethod(), getCallOptions(), request);
    }
  }

  /**
   * <pre>
   * Define the service containing methods here
   * </pre>
   */
  public static final class AuthServiceRoutesFutureStub extends io.grpc.stub.AbstractFutureStub<AuthServiceRoutesFutureStub> {
    private AuthServiceRoutesFutureStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected AuthServiceRoutesFutureStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new AuthServiceRoutesFutureStub(channel, callOptions);
    }

    /**
     * <pre>
     * Basic function call, makes request and returns value
     * </pre>
     */
    public com.google.common.util.concurrent.ListenableFuture<com.example.authclient.OauthCredentials> login(
        com.example.authclient.AccountCredentials request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getLoginMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<com.example.authclient.OauthCredentials> logout(
        com.example.authclient.OauthCredentials request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getLogoutMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_LOGIN = 0;
  private static final int METHODID_LOGOUT = 1;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final AuthServiceRoutesImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(AuthServiceRoutesImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_LOGIN:
          serviceImpl.login((com.example.authclient.AccountCredentials) request,
              (io.grpc.stub.StreamObserver<com.example.authclient.OauthCredentials>) responseObserver);
          break;
        case METHODID_LOGOUT:
          serviceImpl.logout((com.example.authclient.OauthCredentials) request,
              (io.grpc.stub.StreamObserver<com.example.authclient.OauthCredentials>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (AuthServiceRoutesGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .addMethod(getLoginMethod())
              .addMethod(getLogoutMethod())
              .build();
        }
      }
    }
    return result;
  }
}
