package com.example.authclient

import com.example.authclient.AuthServiceRoutesGrpc.getServiceDescriptor
import io.grpc.CallOptions
import io.grpc.CallOptions.DEFAULT
import io.grpc.Channel
import io.grpc.Metadata
import io.grpc.MethodDescriptor
import io.grpc.ServerServiceDefinition
import io.grpc.ServerServiceDefinition.builder
import io.grpc.ServiceDescriptor
import io.grpc.Status
import io.grpc.Status.UNIMPLEMENTED
import io.grpc.StatusException
import io.grpc.kotlin.AbstractCoroutineServerImpl
import io.grpc.kotlin.AbstractCoroutineStub
import io.grpc.kotlin.ClientCalls
import io.grpc.kotlin.ClientCalls.unaryRpc
import io.grpc.kotlin.ServerCalls
import io.grpc.kotlin.ServerCalls.unaryServerMethodDefinition
import io.grpc.kotlin.StubFor
import kotlin.String
import kotlin.coroutines.CoroutineContext
import kotlin.coroutines.EmptyCoroutineContext
import kotlin.jvm.JvmOverloads
import kotlin.jvm.JvmStatic

/**
 * Holder for Kotlin coroutine-based client and server APIs for authService.AuthServiceRoutes.
 */
object AuthServiceRoutesGrpcKt {
  const val SERVICE_NAME: String = AuthServiceRoutesGrpc.SERVICE_NAME

  @JvmStatic
  val serviceDescriptor: ServiceDescriptor
    get() = AuthServiceRoutesGrpc.getServiceDescriptor()

  val loginMethod: MethodDescriptor<AccountCredentials, OauthCredentials>
    @JvmStatic
    get() = AuthServiceRoutesGrpc.getLoginMethod()

  val logoutMethod: MethodDescriptor<OauthCredentials, OauthCredentials>
    @JvmStatic
    get() = AuthServiceRoutesGrpc.getLogoutMethod()

  /**
   * A stub for issuing RPCs to a(n) authService.AuthServiceRoutes service as suspending coroutines.
   */
  @StubFor(AuthServiceRoutesGrpc::class)
  class AuthServiceRoutesCoroutineStub @JvmOverloads constructor(
    channel: Channel,
    callOptions: CallOptions = DEFAULT
  ) : AbstractCoroutineStub<AuthServiceRoutesCoroutineStub>(channel, callOptions) {
    override fun build(channel: Channel, callOptions: CallOptions): AuthServiceRoutesCoroutineStub =
        AuthServiceRoutesCoroutineStub(channel, callOptions)

    /**
     * Executes this RPC and returns the response message, suspending until the RPC completes
     * with [`Status.OK`][Status].  If the RPC completes with another status, a corresponding
     * [StatusException] is thrown.  If this coroutine is cancelled, the RPC is also cancelled
     * with the corresponding exception as a cause.
     *
     * @param request The request message to send to the server.
     *
     * @param headers Metadata to attach to the request.  Most users will not need this.
     *
     * @return The single response from the server.
     */
    suspend fun login(request: AccountCredentials, headers: Metadata = Metadata()): OauthCredentials
        = unaryRpc(
      channel,
      AuthServiceRoutesGrpc.getLoginMethod(),
      request,
      callOptions,
      headers
    )
    /**
     * Executes this RPC and returns the response message, suspending until the RPC completes
     * with [`Status.OK`][Status].  If the RPC completes with another status, a corresponding
     * [StatusException] is thrown.  If this coroutine is cancelled, the RPC is also cancelled
     * with the corresponding exception as a cause.
     *
     * @param request The request message to send to the server.
     *
     * @param headers Metadata to attach to the request.  Most users will not need this.
     *
     * @return The single response from the server.
     */
    suspend fun logout(request: OauthCredentials, headers: Metadata = Metadata()): OauthCredentials
        = unaryRpc(
      channel,
      AuthServiceRoutesGrpc.getLogoutMethod(),
      request,
      callOptions,
      headers
    )}

  /**
   * Skeletal implementation of the authService.AuthServiceRoutes service based on Kotlin
   * coroutines.
   */
  abstract class AuthServiceRoutesCoroutineImplBase(
    coroutineContext: CoroutineContext = EmptyCoroutineContext
  ) : AbstractCoroutineServerImpl(coroutineContext) {
    /**
     * Returns the response to an RPC for authService.AuthServiceRoutes.Login.
     *
     * If this method fails with a [StatusException], the RPC will fail with the corresponding
     * [Status].  If this method fails with a [java.util.concurrent.CancellationException], the RPC
     * will fail
     * with status `Status.CANCELLED`.  If this method fails for any other reason, the RPC will
     * fail with `Status.UNKNOWN` with the exception as a cause.
     *
     * @param request The request from the client.
     */
    open suspend fun login(request: AccountCredentials): OauthCredentials = throw
        StatusException(UNIMPLEMENTED.withDescription("Method authService.AuthServiceRoutes.Login is unimplemented"))

    /**
     * Returns the response to an RPC for authService.AuthServiceRoutes.Logout.
     *
     * If this method fails with a [StatusException], the RPC will fail with the corresponding
     * [Status].  If this method fails with a [java.util.concurrent.CancellationException], the RPC
     * will fail
     * with status `Status.CANCELLED`.  If this method fails for any other reason, the RPC will
     * fail with `Status.UNKNOWN` with the exception as a cause.
     *
     * @param request The request from the client.
     */
    open suspend fun logout(request: OauthCredentials): OauthCredentials = throw
        StatusException(UNIMPLEMENTED.withDescription("Method authService.AuthServiceRoutes.Logout is unimplemented"))

    final override fun bindService(): ServerServiceDefinition = builder(getServiceDescriptor())
      .addMethod(unaryServerMethodDefinition(
      context = this.context,
      descriptor = AuthServiceRoutesGrpc.getLoginMethod(),
      implementation = ::login
    ))
      .addMethod(unaryServerMethodDefinition(
      context = this.context,
      descriptor = AuthServiceRoutesGrpc.getLogoutMethod(),
      implementation = ::logout
    )).build()
  }
}
